<?php /*Template name: inicio*/ ?>
<?php get_header(); ?>
<?php if( have_posts() ) : while ( have_posts() ) : the_post(); ?>
   <main class="index">
      <div class="media">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/0VQEJARHJ44?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="text">
        <h1>Pide, compra, comparte...</h1>
        <div class="alt-txt">
          <h3>Llévale un plato de pasta a quien más lo necesita</h3>
          <p>¿Cuantas pastas pendientes quieres donar?</p>
        </div>
      </div>
      <div class="order">
          <?php echo do_shortcode('[add_to_cart id="8"]'); ?>
<!--        <button class="compra"><i class="flaticon-001-donation">Ordenar</i></button>-->
      </div>
    </main>
<?php endwhile; else : ?>
<p><?php _e( 'Disculpa, no encontramos lo que buscas' ); ?></p>
<?php endif; wp_reset_query();?>
<?php get_footer(); ?>
