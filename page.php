<?php get_header(); ?>
<?php if( have_posts() ) : while ( have_posts() ) : the_post(); ?>
   <main class="page">
     <article>
       <?php the_content(); ?>
     </article>
   </main>
<?php endwhile; else : ?>
<p><?php _e( 'Disculpa, no encontramos lo que buscas' ); ?></p>
<?php endif; wp_reset_query();?>
<?php get_footer(); ?>
