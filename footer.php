 <footer>
   <img src="<?php print IMAGES; ?>logomorandi.svg" alt="" class="logo" draggable="false">
    <div class="socialhub">
      <span>#pastapendiente</span>
      <a href="http://facebook.com/pastamorandi" target="_BLANK"><i class="flaticon-facebook-logo-outline"></i></a>
      <a href="http://twitter.com/pastamorandi" target="_BLANK"><i class="flaticon-twitter-social-outlined-logo"></i></a>
      <a href="http://instagram.com/pastamorandi" target="_BLANK"><i class="flaticon-instagram-social-outlined-logo"></i></a>
    </div>
</footer>
 <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In  -->
<svg version="1.1"
	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
	 x="0px" y="0px" width="749.9px" height="1128.8px" viewBox="0 0 749.9 1128.8" style="enable-background:new 0 0 749.9 1128.8;"
	 xml:space="preserve" class="leaf">
    <style type="text/css">
        .st0{opacity:0.14;}
        .st1{fill:#A0A61B;}
    </style>
    <defs>
    </defs>
    <g transform="translate(2127.65 -52.4)" class="st0 leaf">
        <path class="st1" d="M-1561.5,308.7c30.5-140.5-27.6-256.3-27.6-256.3s-134.2,122.1-164.9,235c-30.5,112.9,21.3,235,21.3,235
            S-1592.3,449.2-1561.5,308.7z"/>
        <path class="st1" d="M-1818.1,644.5c0,0,44.2-171.8-13.8-239s-172.4-78.4-172.4-78.4s-42.8,137.3,21.3,216.6
            C-1918.9,623-1818.1,644.5-1818.1,644.5z"/>
        <path class="st1" d="M-1910,745.1c-61.5-73.3-210-67.3-210-67.3s-30.5,171,30.4,229c60.9,58,195.4,85.3,195.4,85.3
            S-1848.5,818.3-1910,745.1z"/>
        <path class="st1" d="M-1605.8,479.9c-56.3,62.6-80.2,197.7-80.2,197.7s157.4,23.3,229-40.2s79.3-194.2,79.3-194.2
            S-1549.5,417.6-1605.8,479.9z"/>
        <path class="st1" d="M-1700.6,832.7c-56.3,62.6-80.2,197.7-80.2,197.7s157.4,23.3,229-40.2c71.6-63.5,79.3-194.2,79.3-194.2
            S-1644.3,770.1-1700.6,832.7z"/>
        <path class="st1" d="M-1741.9,598.8c-6.3-1.4-13.2,2.6-14.7,8.9l-133,558.5c-1.4,6.6,2.6,13.2,8.9,14.7c0.9,0.2,1.9,0.3,2.9,0.3
            c5.7,0,10.6-3.9,11.8-9.5l133-558.5C-1731.3,607.2-1735.3,600.6-1741.9,598.8z"/>
    </g>

</svg>

  </body>
  <?php wp_footer(); ?>
</html>
