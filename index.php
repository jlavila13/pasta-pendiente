<?php get_header(); ?>
<?php if( have_posts() ) : while ( have_posts() ) : the_post(); ?>
   <main class="index">
      <div class="media">
         <iframe src="https://drive.google.com/file/d/0B30D3NqjMtYbT1daTno5OHg1U2s/preview" autoplay></iframe>
      </div>
      <div class="text">
        <h1>¡Donde come uno podemos comer todos!</h1>
        <div class="alt-txt">
          <h3>Llévale un plato de pasta a quien más lo necesita</h3>
          <p>¿con cuantas pasta pendientes nos quieres ayudar?</p>
        </div>
      </div>
      <div class="order">
          <?php echo do_shortcode('[add_to_cart id="8"]'); ?>
<!--        <button class="compra"><i class="flaticon-001-donation">Ordenar</i></button>-->
      </div>
    </main>
<?php endwhile; else : ?>
<p><?php _e( 'Disculpa, no encontramos lo que buscas' ); ?></p>
<?php endif; wp_reset_query();?>
<?php get_footer(); ?>
