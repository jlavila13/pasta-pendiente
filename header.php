<html>
  <head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="HandheldFriendly" content="true">
	<title><?php wp_title( '&laquo;', true, 'right' ); ?><?php bloginfo('name'); ?></title>
	<link rel="shortcut icon" href="<?php print IMAGES; ?>favico.png" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
<!--	<link rel="stylesheet" href="<?php print SCRIPTS; ?>material.min.css">-->
	<link rel="stylesheet" href=" <?php print TEMPPATH; ?>/fonts/flaticon.css">
	<script src="https://use.fontawesome.com/5ed2ab4a08.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<!--	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.light_green-teal.min.css" />-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<!--	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
	<script src="<?php print SCRIPTS; ?>custom.js"></script>
  <!-- Chrome, Firefox OS, Opera and Vivaldi -->
	<meta name="theme-color" content="#8B9441">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#8B9441">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#8B9441">
	<?php wp_head(); ?>
	<div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.9&appId=585252201665145";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-98278426-1', 'auto');
      ga('send', 'pageview');

    </script>
    <script>
        var etiqueta = $('.from-row').find('label');
        etiqueta.next().insertAfter(etiqueta);
    </script>
  </head>
  <body>
  <header>
    <a href="<?php echo get_option('home'); ?>"><img src="<?php print IMAGES; ?>pp-logo-01.svg" alt="Pasta pendiente"></a>
  </header>

